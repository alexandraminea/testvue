import Vue from 'vue'
import App from './App.vue'
import * as Sentry from "@sentry/vue";
import { Integrations } from "@sentry/tracing";
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

const routes = [
  { path: '/',
    name: 'HelloWorld',
    component: HelloWorld
  }
]

const router = new Router({
  routes: routes
})

Vue.config.productionTip = false

Sentry.init({
  Vue,
  dsn: "https://e089c78223234ffa9a23cbf1a06e37af@o1086579.ingest.sentry.io/6098856",
  integrations: [
    new Integrations.BrowserTracing({
      routingInstrumentation: Sentry.vueRouterInstrumentation(router),
      tracingOrigins: ["localhost", "https://testvue-app.herokuapp.com/", /^\//],
    }),
  ],
  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});

new Vue({
  router,
  render: h => h(App),
}).$mount("#app");